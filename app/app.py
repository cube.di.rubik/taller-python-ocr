from flask import Flask, request, jsonify
from ocr import get_image, process_image
from config import DevelopmentConfig
from models import Ocr, create_tables, database
from flask_cors import CORS, cross_origin

app = Flask(__name__)
app.config.from_object(DevelopmentConfig)
CORS(app)

_VERSION = 1 #API version


@app.route('/')
def index():
	return 'API for OCR by Diego'


@app.route('/v{}/ocr'.format(_VERSION), methods=['POST'])
def ocr_from_url():
	try:
		url = request.form['url']
		image = get_image(url)
		output = process_image(image)
		
		ocr = Ocr.create(
	    	url 	=url,
	    	output	=output,
	    )

		return jsonify({
			'output' : output
		})
	except Exception as e:
		return jsonify({
			'error' : 'Enviaste una URL correcta?'
		})


@app.route('/v{}/ocr'.format(_VERSION), methods=['GET'])
def get_image_from_word():
	images = []
	word = request.args.get('word')
	for image in Ocr.select():
		if word in image.output:
			images.append(image.url)
	return jsonify({
		'images' : images
	})


@app.errorhandler(404)
def page_not_found(error):
	return "Page not found!", 404


@app.errorhandler(500)
def server_internal_error(error):
	return "Error interno en el servidor!", 500


if __name__ == '__main__':
	#create_tables()
	app.run(host='0.0.0.0')


#@app.route('/v{}/process-image'.format(_VERSION))
#def process_image():
#	url = request.args.get('url', 'empty')
#	image = get_image(url)
#	output = pi(image)
#	return output