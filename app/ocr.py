import pytesseract
import requests
from PIL import Image
from PIL import ImageFilter
from io import BytesIO


def process_image(image):
    image.filter(ImageFilter.SHARPEN)
    return pytesseract.image_to_string(image)


def get_image(url):
	image = requests.get(url).content
	stream_image = BytesIO(image)
	return Image.open(stream_image)