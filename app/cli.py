from ocr import get_image, process_image

if __name__ == '__main__':
    """Tool to test the raw output of pytesseract with a given input URL"""
    print("Cliente simple de OCR")
    url = input("¿Cuál es la URL de la imagen que te gustaría analizar?\n")
    image = get_image(url)
    
    print("La salida en bruto de Tesseract sin procesar es:")
    print("-----------------BEGIN-----------------")
    print(process_image(image) + "")
    print("------------------END------------------")