from peewee import *

database = SqliteDatabase('ocr.db')

class Ocr(Model):
    url = CharField()
    output = CharField()

    class Meta:
        database = database # This model uses the "Ocr.database" database.


def create_tables():
    database.connect()
    database.create_tables([Ocr])